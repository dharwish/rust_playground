## Setting up Cargo
1. Creating a directory which you are gonna work with RUST project
```console
mkdir rust_playground
cd rust_playground
## else you can simply create a project using 
cargo new rust_playground
cd rust_playground
```
2. (Skip if you did above 'cargo new ') Initializing the Project Directory by using the command  
```console
cargo init
```
3. Now cargo will create some dependend folders and files for our Projcet, which looks like this
```
rust_playground
│   README.md
│   .gitignore
|   Cargo.toml   
└───src
    │   main.rs
```
4. In 'src' directory the main programe file exist , change directory to src 
```console
cd src
```
5. Open main.rs with prefferd text editor or IDE or open entire 'rust_playground' directory with your IDE.
6. main.rs contains simple programme pre coded.
```rust
fn main(){ 
    println!("Hello World");
}
```

7. To compile and run you programme written on main.rs follow this command.
```console
cargo run
```
Output :
```console
[whoami@swift ~/Projects/rust_playground/src]$ cargo run
   Compiling rust_playground v0.1.0 (/home/whoami/Projects/rust_playground)
    Finished dev [unoptimized + debuginfo] target(s) in 0.92s
         Running `/home/whoami/Projects/rust_playground/target/debug/rust_playground`
Hello, world!
```