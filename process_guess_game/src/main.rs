use std::io;
fn main(){
    println!("Guess the number!");
    //Here we are trying to input from user to a string standerd input output, we will be using io function fromn stabderd  library for the purpose
    println!("Please enter your number");
    //to build mutable variable swe have to start with 'let mut'
    let mut guess = String::new();
    let mut g1 = String::new();
    io::stdin().read_line(&mut g1);
    io::stdin().read_line(&mut guess)
        .expect("Failed to read the line");
    println!("You guessed {}{}", guess,g1);
}