## Creating a process guessing game using basic RUST language syntax
Example code : 
```rust 
use std::io;

fn main() {
    println!("Guess the number!");

    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}
```
#### 1. User input standard library
```rust
use std::io;
```
In this part we are trying to input from user to a string, we will be using function called 'io' from stabderd  library for the purpose.\
To call a library we use : ```use``` command, and we are calling ```io``` library from ```std``` (standard) library by using the following pattern ```use std::io```.

#### 2. Basic print function
```rust   
println!("Guess the number!");
println!("Please input your guess.");
```
